--------------------------------------------
--     Personal nvim lua configuration    --
-- Taken from Kickstart.nvim and modified --
--------------------------------------------

-- Disable vim for long lines
local file_name = vim.api.nvim_buf_get_name(0)
local file = io.open(file_name, "rb")
if file then
  local line = file:read()
  if line and #line > 2048 then
    print('First line too long which might be problematic for some plugins')
    vim.fn.input('Try opening file using nvim --clean')
    vim.cmd('exit')
  end
end

-- Set <\> as the leader key
-- See `:help mapleader`
-- NOTE: Must happen before plugins are loaded (otherwise wrong leader will be used)
vim.g.mapleader = "\\"
vim.g.maplocalleader = "\\"

-- Set to true if you have a Nerd Font installed
vim.g.have_nerd_font = true

-- Match parentheses
vim.g.loaded_matchparen = false

-- Disable syntax for long lines
vim.g.synmaxcol = 1000

-- Python trailing space coloring
vim.g.python_highlight_space_errors = false

---------------------
-- Setting options --
---------------------
-- See `:help vim.opt`
-- For more options, you can see `:help option-list`

-- Make line numbers default
vim.opt.number = true

-- Show absoulte line numbers
vim.opt.relativenumber = false

-- Disable mouse
vim.opt.mouse = ""

-- Don't show vim the mode
vim.opt.showmode = true

-- Sync clipboard between OS and Neovim.
vim.opt.clipboard = "unnamedplus"

-- Enable break indent
vim.opt.breakindent = true

-- Save undo history
vim.opt.undofile = true

-- Case-insensitive searching UNLESS \C or capital in search
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Hide side bar when there is no sign to desplay (error, warning, ...)
vim.opt.signcolumn = "auto"

-- Decrease update time (delay to apply hotkeys for example)
vim.opt.updatetime = 350
vim.opt.timeoutlen = 2000

-- Configure how new splits should be opened
vim.opt.splitright = false
vim.opt.splitbelow = false

-- Sets how neovim will display certain whitespace in the editor.
--  See `:help 'list'`
--  and `:help 'listchars'`
vim.opt.list = false
vim.opt.listchars = { tab = "  ", trail = "·", nbsp = "␣" }

-- Preview substitutions live, as you type!
vim.opt.inccommand = "split"

-- Show which line your cursor is on
vim.opt.cursorline = false

-- Minimal number of screen lines to keep above and below the cursor.
vim.opt.scrolloff = 5

-- Reindenting when typed
vim.opt.cinkeys = "0{,0},0),0],0#,!^F,o,O,e"

-- Smart indenting when entering a new line
vim.opt.smartindent = false

-- Maximum number of items shown in popup menu
vim.opt.pumheight = 10

-- Height of the help menu
vim.opt.helpheight = 90

-- Status line options
vim.opt.laststatus = 0

-- Enable true colors
vim.opt.termguicolors = true

-- Add gray column
vim.opt.colorcolumn = '80'

-- Default shift width (this seems to do nothing)
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4

-------------------
-- Basic Keymaps --
-------------------
-- See `:help vim.keymap.set()`

-- Set highlight on search, but clear on pressing <Esc> in normal mode
vim.opt.hlsearch = true
vim.keymap.set("n", "Q", "<cmd>nohlsearch<CR>")

-- Diagnostic keymaps
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous [D]iagnostic message" })
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next [D]iagnostic message" })
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { desc = "Show diagnostic [E]rror messages" })
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { desc = "Open diagnostic [Q]uickfix list" })

-- Use CTRL+<hjkl> to switch between windows
vim.keymap.set("n", "<C-h>", "<C-w><C-h>", { desc = "Move focus to the left window" })
vim.keymap.set("n", "<C-l>", "<C-w><C-l>", { desc = "Move focus to the right window" })
vim.keymap.set("n", "<C-j>", "<C-w><C-j>", { desc = "Move focus to the lower window" })
vim.keymap.set("n", "<C-k>", "<C-w><C-k>", { desc = "Move focus to the upper window" })

-- Use <jk> to move up and down on long lines with soft line break
vim.keymap.set("n", "k", "gk", { desc = "Move down soft line break normal" })
vim.keymap.set("n", "j", "gj", { desc = "Move up soft line break normal" })
vim.keymap.set("v", "k", "gk", { desc = "Move down soft line break visual" })
vim.keymap.set("v", "j", "gj", { desc = "Move up soft line break visual" })

-- Use \f to format code
vim.keymap.set("n", "<leader>f", vim.lsp.buf.format, { desc = "Format code" })

------------------------
-- Basic Autocommands --
------------------------
-- See `:help lua-guide-autocommands`

-- Highlight when yanking (copying) text
-- Try it with `yap` in normal mode
-- See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd("TextYankPost", {
  desc = "Highlight when yanking (copying) text",
  group = vim.api.nvim_create_augroup("kickstart-highlight-yank", { clear = true }),
  callback = function()
    vim.highlight.on_yank()
  end,
})

-- Restore cursor position
vim.api.nvim_create_autocmd({ "BufReadPost" }, {
  pattern = { "*" },
  callback = function()
    vim.api.nvim_exec('silent! normal! g`"zv', false)
  end,
})

-- Install `lazy.nvim` plugin manager
-- See `:help lazy.nvim.txt` or https://github.com/folke/lazy.nvim for more info
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
end

---@diagnostic disable-next-line: undefined-field
vim.opt.rtp:prepend(lazypath)

-----------------------------------
-- Configure and install plugins --
-----------------------------------
-- To check the current status of your plugins, run
-- :Lazy
require("lazy").setup({
  -- Plugins can also be added by using a table,
  -- with the first argument being the link and the following
  -- keys can be used to configure plugin behavior/loading/etc.
  --
  -- Use `opts = {}` to force a plugin to be loaded.
  -- This is equivalent to:
  -- require('Comment').setup({})

  -- Plugins can specify dependencies.
  -- Use the `dependencies` key to specify the dependencies of a particular plugin

  { -- Fuzzy Finder (files, lsp, etc)
    "nvim-telescope/telescope.nvim",
    version = "0.1.x",
    event = "VimEnter",
    dependencies = {
      "nvim-lua/plenary.nvim",
      {
        -- If encountering errors, see telescope-fzf-native README for install instructions
        "nvim-telescope/telescope-fzf-native.nvim",

        -- `build` is used to run some command when the plugin is installed/updated.
        -- This is only run then, not every time Neovim starts up.
        build = "make",

        -- `cond` is a condition used to determine whether this plugin should be
        -- installed and loaded.
        cond = function()
          return vim.fn.executable("make") == 1
        end,
      },
      { "nvim-telescope/telescope-ui-select.nvim" },

      -- Useful for getting pretty icons, but requires a Nerd Font.
      {
        "nvim-tree/nvim-web-devicons",
        enabled = vim.g.have_nerd_font
      },
    },
    config = function()
      -- See `:help telescope` and `:help telescope.setup()`
      local actions = require("telescope.actions")
      require("telescope").setup({
        -- pickers = {}
        extensions = {
          ["ui-select"] = {
            require("telescope.themes").get_dropdown(),
          },
        },
        defaults = {
          mappings = {
            i = {
              ["<esc>"] = actions.close,
            },
          },
        }
      })

      -- Enable telescope extensions, if they are installed
      pcall(require("telescope").load_extension, "fzf")
      pcall(require("telescope").load_extension, "ui-select")

      -- See `:help telescope.builtin`
      local builtin = require("telescope.builtin")
      vim.keymap.set("n", "<C-p>", builtin.find_files, { desc = "[S]earch [F]iles" })
      vim.keymap.set("n", "<leader>sk", builtin.keymaps, { desc = "[S]earch [K]eymaps" })
      vim.keymap.set("n", "<leader>sp>", builtin.find_files, { desc = "[S]earch [F]iles" })
      vim.keymap.set("n", "<leader>ss", builtin.builtin, { desc = "[S]earch [S]elect Telescope" })
      vim.keymap.set("n", "<leader>sw", builtin.grep_string, { desc = "[S]earch current [W]ord" })
      vim.keymap.set("n", "<leader>sg", builtin.live_grep, { desc = "[S]earch by [G]rep" })
      vim.keymap.set("n", "<leader>sd", builtin.diagnostics, { desc = "[S]earch [D]iagnostics" })
      vim.keymap.set("n", "<leader>sr", builtin.resume, { desc = "[S]earch [R]esume" })
      vim.keymap.set("n", "<leader>s.", builtin.oldfiles, { desc = '[S]earch Recent Files ("." for repeat)' })
      -- vim.keymap.set("n", "<leader><leader>", builtin.buffers, { desc = "[ ] Find existing buffers" })

      -- Shortcut for searching your neovim configuration files
      vim.keymap.set("n", "<leader>sn", function()
        builtin.find_files({ cwd = vim.fn.stdpath("config") })
      end, { desc = "[S]earch [N]eovim files" })
    end,
  },

  { -- LSP Configuration & Plugins
    "neovim/nvim-lspconfig",
    lazy = false,
    priority = 950,
    dependencies = {
      -- Automatically install LSPs and related tools to stdpath for neovim
      "williamboman/mason.nvim",
      "williamboman/mason-lspconfig.nvim",
      "WhoIsSethDaniel/mason-tool-installer.nvim",
      { "j-hui/fidget.nvim", opts = {} },
    },
    config = function()
      vim.api.nvim_create_autocmd("LspAttach", {
        group = vim.api.nvim_create_augroup("kickstart-lsp-attach", { clear = true }),
        callback = function(event)
          -- Disable semantic tokens
          local client = vim.lsp.get_client_by_id(event.data.client_id)
          client.server_capabilities.semanticTokensProvider = nil

          local map = function(keys, func, desc)
            vim.keymap.set("n", keys, func, { buffer = event.buf, desc = "LSP: " .. desc })
          end

          map("gd", require("telescope.builtin").lsp_definitions, "[G]oto [D]efinition")
          map("gr", require("telescope.builtin").lsp_references, "[G]oto [R]eferences")
          map("gI", require("telescope.builtin").lsp_implementations, "[G]oto [I]mplementation")
          map("<leader>rn", vim.lsp.buf.rename, "[R]e[n]ame")
          map("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")
          map("K", vim.lsp.buf.hover, "Hover Documentation")
          map("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
          map('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')
          vim.cmd [[autocmd! ColorScheme * highlight NormalFloat guibg=#1f2335]]
          vim.cmd [[autocmd! ColorScheme * highlight FloatBorder guifg=white guibg=#1f2335]]
        end,
      })

      local capabilities = vim.lsp.protocol.make_client_capabilities()
      capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())

      -- See `:help lspconfig-all` for a list of all the pre-configured LSPs
      local servers = {

        -- C/C++
        clangd = {
          cmd = {
            "clangd",
            "--background-index",
            "--suggest-missing-includes",
            "--function-arg-placeholders=0",
            "--header-insertion=never",
            "--log=verbose",
            -- TODO clang-tidy always seem to run, check why
            "--clang-tidy",
          },
          enabled = true,
          root_dir = function(fname)
            local util = require('lspconfig.util')
            return util.root_pattern(unpack({ ".clang-format" }))(fname) or util.find_git_ancestor(fname)
          end
        },

        -- Python (requires python3.x-venv)
        pyright = {},
        yapf = {},

        -- Rust
        rust_analyzer = {},

        -- Asm
        asmfmt = {},

        -- Bash/sh
        bashls = {},
        shfmt = {},
        shellcheck = {},

        -- Json
        jsonls = {},
        jq = {},

        -- Yaml
        -- Use yamllint and yamlfmt
        yamlls = {},

        -- CMake
        -- Use cmake-lint and cmake-format

        -- Meson
        swift_mesonls = {},

        -- Lua
        lua_ls = {
          settings = {
            Lua = {
              runtime = { version = "LuaJIT" },
              workspace = {
                checkThirdParty = false,
                -- Tells lua_ls where to find all the Lua files that you have loaded
                -- for your neovim configuration.
                library = {
                  "${3rd}/luv/library",
                  unpack(vim.api.nvim_get_runtime_file("", true)),
                },
              },
              completion = {
                callSnippet = "Replace",
              },
            },
          },
        },
      }

      -- Ensure the servers and tools above are installed
      -- To check the current status of installed tools and/or manually install
      -- other tools, you can run
      -- :Mason
      require("mason").setup()

      -- You can add other tools here that you want Mason to install
      local ensure_installed = vim.tbl_keys(servers or {})

      -- Used to format lua code
      vim.list_extend(ensure_installed, {
        "stylua",
      })
      require("mason-tool-installer").setup({ ensure_installed = ensure_installed })

      require("mason-lspconfig").setup({
        handlers = {
          function(server_name)
            local server = servers[server_name] or {}
            server.capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {})
            require("lspconfig")[server_name].setup(server)
          end,
        },

      })

      -- Disable diagnostics after editing
      -- Enable it on save after slight delay
      local lsp_diagnostics_hover_id = nil
      local lsp_diagnostics_enable_delay = 1500

      local enable_diagnostics = function(args)
        vim.diagnostic.enable(args.buf)
        lsp_diagnostics_hover_id = vim.api.nvim_create_autocmd({ "CursorHold" }, {
          callback = function()
            vim.diagnostic.open_float(nil, { focus = false })
          end
        })
      end

      local enable_diagnostics_delayed = function(args)
        local timer = vim.loop.new_timer()
        timer:start(lsp_diagnostics_enable_delay, 0,
          vim.schedule_wrap(
            function()
              enable_diagnostics(args)
            end
          )
        )
      end

      local disable_diagnostics = function(args)
        vim.diagnostic.disable(args.buf)
        if lsp_diagnostics_hover_id then
          vim.api.nvim_del_autocmd(lsp_diagnostics_hover_id)
          lsp_diagnostics_hover_id = nil
        end
      end

      vim.api.nvim_create_autocmd({ "BufNew", "TextChanged", "TextChangedI", "TextChangedP", "TextChangedT" }, {
        callback = disable_diagnostics
      })

      vim.api.nvim_create_autocmd({ "BufWrite", "VimEnter" }, {
        callback = enable_diagnostics_delayed
      })

      vim.diagnostic.config {
        underline = true,
        virtual_text = false,
        signs = true,
        update_in_insert = false,

        float = {
          width = 60,
          border = "rounded",
          relative = "cursor",
        },
      }
    end,
  },

  -- Autocompletion, snippets, etc
  {
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
      {
        "L3MON4D3/LuaSnip",
        build = (function()
          if vim.fn.has("win32") == 1 or vim.fn.executable("make") == 0 then
            return
          end
          return "make install_jsregexp"
        end)(),
      },
      "saadparwaiz1/cmp_luasnip",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-path",
    },
    config = function()
      -- See `:help cmp`
      local cmp = require("cmp")
      local luasnip = require("luasnip")
      luasnip.config.setup({})

      cmp.setup({
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end,
        },

        window = {
          documentation = cmp.config.window.bordered(),
          completion = {
            border = "rounded",
            scrolloff = 1,
            side_padding = 0,
            winhighlight = 'Normal:Pmenu,FloatBorder:Pmenu,CursorLine:PmenuSel,Search:None',
          }
        },

        preselect = true,

        completion = {
          completeopt = "menu,menuone,noinsert,noselect",
          keyboard_length = 3,
          pumheight = 10,
        },

        view = {
          docs = {
            auto_open = false,
          }
        },

        formatting = {
          format = function(_, vim_item)
            vim_item.abbr = string.sub(vim_item.abbr or "", 1, 30)
            vim_item.kind = string.sub(vim_item.kind or "", 1, 20)
            vim_item.menu = string.sub(vim_item.menu or "", 1, 30)
            return vim_item
          end
        },

        -- Read `:help ins-completion`, it is really good!
        mapping = cmp.mapping.preset.insert({
          ["<tab>"] = cmp.mapping.select_next_item(),
          ["<C-n>"] = cmp.mapping.select_next_item(),
          ["<C-p>"] = cmp.mapping.select_prev_item(),
          ["<return>"] = cmp.mapping.confirm({ select = true }),

          -- <c-l> will move you to the right of each of the expansion locations.
          ["<C-l>"] = cmp.mapping(function()
            if luasnip.expand_or_locally_jumpable() then
              luasnip.expand_or_jump()
            end
          end, { "i", "s" }),

          -- <c-h> is similar, except moving you backwards.
          ["<C-h>"] = cmp.mapping(function()
            if luasnip.locally_jumpable(-1) then
              luasnip.jump(-1)
            end
          end, { "i", "s" }),
        }),

        sources = {
          { name = "path" },
          { name = "nvim_lsp" },
          { name = "luasnip" },
        },
      })
    end,
  },

  -- Personal colorscheme
  {
    url = 'https://gitlab.com/ado0/nvim_colorscheme.git',
    lazy = false,
    priority = 800,
    config = function()
      vim.cmd.colorscheme('nvim_lua_ado')

      --[[
      -- Hide all semantic highlights if enabled
      for _, group in ipairs(vim.fn.getcompletion("@lsp", "highlight")) do
        vim.api.nvim_set_hl(0, group, {})
      end
      --]]
    end,
  },

  -- Colorize hex values
  {
    "chrisbra/Colorizer"
  },

  -- Show indent
  {
    "Yggdroot/indentLine",
  },

  -- ASCII art
  {
    'vim-scripts/DrawIt',
  },

  -- Surround object
  {
    'tpope/vim-surround'
  },

  -- Better dot op
  {
    'tpope/vim-repeat'
  },

  -- Useful git wrapper
  {
    'tpope/vim-fugitive'
  },

  -- Transparent background
  {
    'xiyaowong/transparent.nvim'
  },

  -- Highlight matching parentheses for a short period
  -- (will probably never use this)
  {
    'andymass/vim-matchup',
    config = function()
      vim.cmd("NoMatchParen")

      vim.keymap.set("n", "<C-m>", function()
        vim.cmd("DoMatchParen")
        local timer = vim.loop.new_timer()
        timer:start(500, 0,
          vim.schedule_wrap(function()
            vim.cmd("NoMatchParen")
          end
          )
        )
      end, { desc = "Highlight matching parentheses for a short period" }
      )
    end
  },

  -- Regex based synax higlighting (fast)
  {
    "sheerun/vim-polyglot",
  },

  -- Color parentheses (not maintained)
  {
    "eapache/rainbow_parentheses.vim",
    lazy = true,
    config = function()
      local colorspairs = {
        { 'darkyellow', 'gray' },
        { 'darkgreen',  'orange' },
        { 'darkcyan',   'firebrick3' },
        { 'Darkblue',   'darkyellow' }
      }

      local rbpt_max = 14
      local rbpt_colorpairs = {}

      for i = 1, rbpt_max, 1 do
        table.insert(rbpt_colorpairs, colorspairs[i % #colorspairs])
      end

      vim.g.rbpt_colorpairs = rbpt_colorpairs
      vim.g.rbpt_max = rbpt_max
      vim.g.bold_parantheses = false

      -- TODO fix this
      vim.api.nvim_create_autocmd({ "VimEnter", "BufEnter", "BufReadPost" }, {
        desc = "Activate rainbow_parentheses",
        group = vim.api.nvim_create_augroup("rainbow_parentheses", { clear = true }),
        callback = function()
          vim.api.nvim_exec('RainbowParenthesesLoadRound', false)
          vim.api.nvim_exec('RainbowParenthesesLoadSquare', false)
          vim.api.nvim_exec('RainbowParenthesesLoadBraces', false)
          vim.api.nvim_exec('RainbowParenthesesActivate', false)
        end
      })
    end
  },

  -------------------------
  -- Interesting plugins --
  -------------------------
  -- "folke/todo-comments.nvim",

  -- Disable clangd before using this 
  -- Not supported by mason yet
  -- Completion not configured
  --[[
  {
    "ranjithshegde/ccls.nvim",
    config = function()
      local lspconfig = require 'lspconfig'
      lspconfig.ccls.setup {
        cmd = { 'ccls' },
        rootPatterns = { ".ccls", "compile_commands.json", ".vim/", ".git/", "hg" },
        init_options = {
          -- compilationDatabaseDirectory = "build",
          clang = {
            excludeArgs = { "-frounding-math" },
          },
          lsp = {
            use_defaults = true,
          },
          cache = {
            directory = vim.fn.expand("$HOME/.ccls-cache"),
            retainInMemory = 1
          },
          diagnostics = {
            onOpen = 0,
            onChange = 1000,
            onSave = 0
          },
          index = {
            threads = 4
          }
        }
      }
    end
  },
  --]]

  -- Very slow ...
  -- Highlight, edit, and navigate code
  --[[
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    lazy = true,
    priority = 900,
    config = function()
      ---@diagnostic disable-next-line: missing-fields
      require("nvim-treesitter.configs").setup({
        ensure_installed = { "bash", "c", "cpp", "rust", "lua", "markdown", "vim", "vimdoc" },
        auto_install = true,
        highlight = { enable = false, },
      })

      vim.api.nvim_create_autocmd({ "VimEnter" }, {
        desc = "Activate treesitter rainbow parentheses",
        group = vim.api.nvim_create_augroup("treesitter_rainbow_parentheses", { clear = true }),
        callback = function()
          ---@diagnostic disable-next-line: missing-fields
          require("nvim-treesitter.configs").setup({
            rainbow = { enable = false, },
          })
        end
      })
    end,
    dependencies = { "HiPhish/nvim-ts-rainbow2" },
    opts = function(_, opts)
      opts.rainbow = {
        enable = false,
        query = "rainbow-parens",
        strategy = require("ts-rainbow").strategy.global,
        -- TODO set colors
      }
    end,
  },
  --]]

}, {
  ui = {
    -- If you have a Nerd Font, set icons to an empty table which will use the
    -- default lazy.nvim defined Nerd Font icons otherwise define a unicode icons table
    icons = vim.g.have_nerd_font and {} or {
      cmd = "⌘",
      config = "🛠",
      event = "📅",
      ft = "📂",
      init = "⚙",
      keys = "🗝",
      plugin = "🔌",
      runtime = "💻",
      require = "🌙",
      source = "📄",
      start = "🚀",
      task = "📌",
      lazy = "💤 ",
    },
  },
})

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
